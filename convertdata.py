#convert tick data to second data
from datetime import datetime, timedelta
def gettime(line):
    parts = line.split(",")
    return datetime.strptime(parts[1],"%Y%m%d %H:%M:%S.%f")
filename = "data/EURUSD-2016-01.csv"
lines = open(filename).read().split("\n")
i = 0
m = gettime(lines[0])
moment = datetime(m.year, m.month, m.day, m.hour, m.minute, m.second)
last = gettime(lines[-2])


base, ex = filename.split(".")
out = open(base + "-second." + ex,'w')
while moment <= last:
    #get last tick
    t = gettime(lines[i])
    while t < moment:
        i += 1
        t = gettime(lines[i])
    #write last tick
    out.write(lines[i-1] + "\n")
    moment += timedelta(seconds=1)

    
