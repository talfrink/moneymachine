import zipfile, requests

def download_file(url):
    local_filename = url.split('/')[-1]
    r = requests.get(url, stream=True)
    with open(local_filename, 'wb') as f:
        for chunk in r.iter_content(chunk_size=1024): 
            if chunk: # filter out keep-alive new chunks
                f.write(chunk)
    return local_filename

def getfiles():
    url = "http://truefx.com/dev/data/{year}/{month}-{year}/EURUSD-{year}-{monthn:02d}.zip"
    months = ["JANUARY","FEBRUARY","MARCH","APRIL","MAY","JUNE","JULY","AUGUST","SEPTEMBER","OCTOBER","NOVEMBER","DECEMBER"]
    for year in range(2009,2017):
        for monthn in range(1,13):
            month = months[monthn-1]
            filename = download_file(url.format(**locals()))
            print filename
            try:
                z = zipfile.ZipFile(filename, 'r')
                z.extractall("data")
                print "extracted"
            except zipfile.BadZipfile:
                print "bad"
                continue
                
if __name__ == "__main__":
    getfiles()
