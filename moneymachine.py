from collections import Counter, defaultdict
from datetime import timedelta
from scipy.spatial import cKDTree
from pprint import pprint
from random import randint, random
from math import log
import time

class MoneyMachine(object):
    def __init__(self, dollars=100):
        self.dollars = dollars
        self.euros = 0
        self._initmodel()
    def buyeuros(self, price):
        self.euros += self.dollars / price
        self.dollars = 0
    def selleuros(self, price):
        self.dollars += self.euros * price
        self.euros = 0
    def _bestdecision(self, history, period):
        # what would have been the best decision <period> ago?
        bid, ask = history[-1]
        hbid, hask = history[-(period+1)]
        if hbid > ask: return 1
        elif hask > bid: return -1
        return 0
    def getsettings(self):
        raise NotImplementedError
    def learn(self, history, decision):
        raise NotImplementedError
    def act(self, history):
        raise NotImplementedError
    def _initmodel(self):
        raise NotImplementedError

class NearestNeighborsMoneyMachine(MoneyMachine):
    def __init__(self, 
                 ndimensions = None, 
                 nneighbors = None, 
                 aggression = None,
                 step = None,
                 *args, **kwargs):
        self.ndimensions = ndimensions or randint(1,5)
        self.nneighbors = nneighbors or randint(1,10)
        self.aggression = aggression or randint(50,100)
        self.step = step or randint(100,1000)
        self.histsize = randint(self.step, self.step*self.ndimensions*2)
        super(NearestNeighborsMoneyMachine, self).__init__(*args, **kwargs)

    def getsettings(self):
        return {v:getattr(self,v) for v in [
            "ndimensions",
            "nneighbors",
            "aggression",
            "step",
            "histsize"]}
        
    def _initmodel(self):
        self.iteration = 0
        self.outcomes = defaultdict(lambda:Counter())

    def _getfeatures(self, history):
        #height compared to max and min at various distances
        features = []
        _min,_max = history[-1]
        now = (_min + _max)/2
        step = int(self.histsize / self.ndimensions)
        for i in range(step,self.histsize+1,step):
            h = history[-i:-i+step-1]
            _min = min(_min,min(h)[0])
            _max = max(_max,max(h)[1])
            relative = round((now-_min)/(_max - _min),2)
            features.append(relative)
        return tuple(features)

    def learn(self, history):
        if self.iteration > self.histsize + self.step:
            decision = self._bestdecision(history, self.step)
            features = self._getfeatures(history[-(self.histsize+self.step):-self.step])
            self._learn(features, decision)
        self.iteration += 1

    def _learn(self, features, decision):
        self.outcomes[features][decision] += 1
                
    def act(self, history):
        if self.iteration > self.histsize + self.step:
            if self.iteration % self.step == 0:
                action = self._act(history[-self.histsize:])
                if action <= -(1-self.aggression/100.): self.selleuros(history[-1][0])
                if action >= (1-self.aggression/100.): self.buyeuros(history[-1][1])

    def _act(self, history):
        keys = self.outcomes.keys()
        self.tree = cKDTree(keys)
        home = self._getfeatures(history)
        nindexes = self.tree.query(home, self.nneighbors)[1]
        weights = {-1:0,0:0,1:0}
        if type(nindexes) == int:
            nindexes = [nindexes]
        for i in nindexes:
            if i == len(keys):
                continue
            neighbor = keys[i]
            for d,o in self.outcomes[neighbor].items():
                weights[d] += o
        decision = sum([d*amount for d,amount in weights.items()])
        decision /= float(sum(weights.values()))
        return decision

def iterdata(filename):
    lines = open(filename).read().strip().split("\n")
    data = []
    for i in range(len(lines)):
        bid,ask = map(float,lines[i].split(",")[-2:])
        data.append((bid,ask))
        yield data


def run():
    while True:
        machine = NearestNeighborsMoneyMachine()
        print machine.getsettings()
        broken = False
        for i,history in enumerate(iterdata("data/EURUSD-2016-03-second.csv")):
            machine.learn(history)
            machine.act(history)
            funds = machine.dollars + machine.euros * history[-1][0]
            dayprofit = (funds/100)**(60*60*24/(float(i)+1))
            if i % 3600 == 0:
                print "{}: {}".format(timedelta(seconds=i), dayprofit)
                if dayprofit < 1.0003 and i > 3600*5:
                    broken = True
                    break
        if not broken:
            # made it through the dataset!
            open("win.txt","a").write("{}, {}".format(machine, machine.getsettings()))

if __name__ == "__main__":
    run()
